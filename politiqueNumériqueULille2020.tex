\documentclass[fontsize=9pt,twoside]{scrartcl}
\usepackage{rapportULille}
%\usepackage[contents=Projet,color=lightgray]{background}
\usepackage{lettrine}
\usepackage[normalem]{ulem}

\selectlanguage{french}
%\selectlanguage{english}

\title{Politique du numérique à l'Université de Lille}
\date{}
\author{}
\piedDePage{\textbf{\color{pourpre}Université de Lille} \\
  Vice présidence transformation numérique\\
  42 rue Paul Duez, 59000 Lille}%\\
  %\href{mailto:Pierre.Boulet@univ-lille.fr}{Pierre.Boulet@univ-lille.fr}}

\newcommand{\separe}{\medskip}
\newcommand{\ulille}{l'Université de Lille\xspace}
\newcommand{\Ulille}{L'Université de Lille\xspace}
\newcommand{\dnum}{DGDNum\xspace}
\newcommand{\site}[2][defaultfootnotelabel]{\footnote{\label{#1}\url{#2}}}
\newcommand{\todo}[1]{\noindent\colorbox{yellow}{
  \begin{minipage}{\linewidth-2\fboxsep-2\fboxrule}
  \raggedright #1
  \end{minipage}}}
\newcommand{\cadre}[2]{%
  \begin{figure}[htbp]
  \centering
  \colorbox{pourpre!10}{
    \begin{minipage}{\linewidth-2\fboxsep-2\fboxrule}
       \subsubsection*{#1} #2
    \end{minipage}}
  \end{figure}}
\newcommand{\projet}[4]{\paragraph{#1.} #4\strut\\
\textbf{Niveau de maturité :} #2.\\
\textbf{Échéance :} #3.}

\begin{document}
\maketitle
\vspace{-4\baselineskip}
\vfill
\begin{description}
%  \item[Version :] 1.5 du \today.
  \item[Auteurs :] Pierre Boulet avec des contributions de Didier Lamballais, Cédric Foll, Mostafa Laforge, Philippe Marquet, Nouredine Melab, Pierre Ravaux, Christophe Mondou, Lionel Montagne, Martine Sion
  \item[Approbation :] validée par le conseil d'administration le 19 novembre 2020
  % \item[Étapes de validation :]\strut
  % \begin{enumerate}
  %   \item \sout{chargés de missions et directeurs}
  %   \item \sout{bureau}
  %   \item comité de direction -- 13/11/2020
  %   \item CA -- 19/11/2020
  % \end{enumerate}
  \item[Licence :] Ce texte est mis à disposition selon les termes de la licence Creative Commons Attribution - Partage dans les Mêmes Conditions 4.0 International, \url{http://creativecommons.org/licenses/by-sa/4.0/}.
  \item[Sources \LaTeX :] \strut\\
  \url{https://gitlab.univ-lille.fr/pierre.boulet/politiquenumulille}
\end{description}

\vfill
\tableofcontents
\vfill

\newpage
\strut
\vfill
\centerline{\includegraphics[width=0.8\textwidth]{piliers.pdf}}
\vfill
\newpage

\lettrine{\textbf{\color{pourpre}L}}{\textbf{\color{pourpre}e numérique}} est avant tout un outil, mais aussi un puissant levier de transformation des organisations et de la société. \Ulille ne considère pas le numérique comme une fin en soi, mais comme un outil primordial de transformation au service des grandes transitions de notre époque : environnementale, sociétale, économique et pédagogique.

Dans ce document, nous donnons dans un premier temps les grandes priorités de la politique numérique de \ulille, puis les principes de mise en œuvre de celles-ci. Une annexe vient compléter ce document politique par la liste des grands projets numériques de 2020 et des quelques années suivantes pour tracer la dynamique de la transformation numérique de \ulille.

\section{Grandes priorités}

\subsection{Infrastructure performante et sûre}
Le numérique impacte tous les domaines de l'université (enseignement, recherche, administration). C'est un facteur d'efficacité au travail pour les personnels et de qualité des études pour les étudiants. De nombreux usages se développent et ne sont pleinement possibles que si l'infrastructure numérique est performante. \Ulille fait donc porter un effort important sur cette infrastructure : le réseau, les capacités d'hébergement de puissance de calcul et de stockage, l'équipement audio-visuel des salles d'enseignement, et les logiciels de base. Dans ces logiciels de base, on compte le système d'information (SI) de l'université, les outils de travail collaboratif et les outils supports à l'enseignement au premier rang desquels la plateforme Moodle. Les performances seules ne suffisent pas, il faut aussi de la sûreté de fonctionnement pour que les utilisateurs aient confiance en cette infrastructure et l'utilisent sans craindre une défaillance ou une faille dans la sécurité de leurs données. Ce travail de l'ombre porte à la fois sur la fiabilité de l'infrastructure et sur sa sécurité.

\subsection{Numérique responsable}
Le numérique est une technologie avec une grande matérialité (terminaux, data centres et réseaux). L'ADEME estime\site{https://www.ademe.fr/sites/default/files/assets/documents/guide-pratique-face-cachee-numerique.pdf} qu'en 2020 le numérique est responsable de 4~\% des émissions de gaz à effets de serre mondiales, soit plus que l'aviation civile et que sa part croit de plus de 10~\% par an. Son impact sur l'épuisement des ressources en métaux rares est, lui aussi, très important.

Le numérique responsable consiste à prendre en compte systématiquement les enjeux des impacts environnementaux et sociaux du numérique. Il s'agit en particulier de se demander si les coûts environnementaux et sociaux de chaque projet numérique ne sont pas supérieurs à ses bénéfices.

\Ulille s'engage dans une réduction de l'impact environnemental de ses infrastructures et usages numériques. Cette réduction engage à revisiter toutes les phases du cycle de vie du matériel informatique : de la fabrication (en favorisant l'achat de matériel sobre, réparable, à impact environnemental réduit), à la fin de vie de ce matériel (réparation, réutilisation et seulement après, recyclage), en passant par les usages (conception sobre des services numériques et lutte contre l'obsolescence programmée).

\Ulille s'engage aussi à utiliser les outils numériques là où ils peuvent réduire les impacts environnementaux et sociaux de ses activités, tout en faisant attention à la fracture numérique pour ne pas empêcher des publics éloignés du numérique d'accéder à ses services. La dématérialisation des procédures et l'utilisation de visioconférences permet de réduire les besoins de transport, et donc les impacts associés (pollution, temps perdu). Le télétravail, choisi par les personnels, est aussi un moyen d'amélioration de la qualité de vie au travail et de réduction des impacts liés au transport.

Enfin, \ulille porte une attention renforcée aux impacts de ses projets et infrastructures numériques sur la vie privée des personnes concernées, conformément au RGPD, le réglement général européen pour la protection des données\site{https://www.univ-lille.fr/dp/}.

\subsection{Accompagnement des utilisateurs vers l'autonomie}
L'université est avant tout un lieu de formation et de production de connaissance, et a pour mission de former des citoyens aptes à contribuer à la société du XXI\ieme{} siècle. Or, cette société est et sera numérique. Il est donc particulièrement important de rendre nos étudiants et nos personnels autonomes et capables de tirer le plus grand bénéfice des usages du numérique. Ainsi nous travaillons sur la formation des étudiants et des personnels à la culture et aux compétences numériques (sur la base du référentiel européen DigComp\site{https://ec.europa.eu/jrc/en/digcomp}), et à l'accompagnement de tous dans l'utilisation des outils numériques proposés par l'université pour que le numérique fasse gagner du temps et donc de la qualité de vie à chacun et chacune. L'autonomie des personnels se décline notamment dans la faculté des enseignants à utiliser des outils numériques pour l'hybridation des formations et la mise en œuvre de pédagogies actives et interactives. \Ulille met ainsi en œuvre les outils et les formations nécessaires à la transformation pédagogique au service d'un meilleur apprentissage de ses étudiants.

\medskip
Ces 3 grandes priorités fonctionnent en synergie et en particulier la performance n'est pas contradictoire avec la sobriété. À titre d'exemple, citons l'investissement dans le réseau d'accès optique de \ulille, RAOUL\site{https://raoul.univ-lille.fr/} qui a été réalisé sous la forme d'une location longue durée de fibres optiques, ce qui permet des économies tout en offrant des performances de haut niveau et adaptables au besoin par la mise à jour des équipements actifs de réseau.

\cadre{L'université post-Covid}{
Le confinement forcé par la crise sanitaire due à la Covid-19 a mis en évidence le rôle central des services numériques dans toutes les organisations, et en particulier dans les universités. Tout le monde est passé du jour au lendemain au télétravail et à l'enseignement à distance pour assurer la continuité pédagogique.

Une chronique de cet événement et de la façon dont \ulille s'est adaptée en un temps record à ces contingences est disponible sur le blog du numérique à \ulille\site{https://numerique.univ-lille.fr/numerique/univcovid19}. On peut résumer les multiples effet du confinement sur le numérique dans l'université par une formidable accélération de la transformation numérique. La prise de conscience de la possibilité de mener à bien un grand nombre de nos missions à distance a permis de généraliser l'accès au télétravail qui n'était qu'au stade de l'expérimentation ; la nécessaire réflexion sur les méthodes pédagogiques pour l'enseignement à distance a fait progresser nombre d'enseignants dans leurs pratiques pédagogiques, y compris pour l'enseignement présentiel ; les besoins de dématérialisation des procédures ont accéléré les projets de dématérialisation (inscription des étudiants, signature électronique, etc) ; la nécessité de préparer une rentrée partiellement à distance a permis de débloquer des fonds exceptionnels (avec l'aide importante de l'I-Site) pour l'équipement numérique des salles d'enseignement, des enseignants et des étudiants ; la tenue des conférences internationales à distance a fait prendre conscience de la possibilité d'avoir des échanges scientifiques efficaces sans prendre l'avion.

Cette crise a mis en valeur les qualités fondamentales de solidarité et de créativité de la communauté universitaire, et le numérique a été un catalyseur de ces qualités.
}

\section{Principes de mise en œuvre}
\subsection{Démarche centrée utilisateurs}
Les projets numériques menés par la \dnum (direction générale déléguée au numérique) de \ulille sont pour partie des projets menés sous l'impulsion de cette direction, mais aussi, et dans leur majorité, portés par un besoin exprimé par des utilisateurs. Ces utilisateurs peuvent être des personnels des composantes, des laboratoires ou des directions administratives, ou encore des étudiants. Au-delà de l'expression des besoins, les utilisateurs sont associés à toutes les étapes des projets : la conception, la réalisation, le déploiement et la maintenance. Cette démarche systématique vise à assurer que les logiciels et services mis en place correspondent bien au besoin des utilisateurs dans la durée.

Suivant cette logique de répondre aux besoins de tous les utilisateurs (priorité numérique responsable), \ulille porte une attention particulière à l'accessibilité de ses services numériques, en particulier sur le web\site{https://www.univ-lille.fr/accessibilite/} en respectant systématiquement le RGAA (référentiel général d'accessibilité pour les administrations).

\subsection{Mutualisation}
Les établissements d'enseignement supérieur et de recherche partagent un grand nombre de besoins numériques. Leur SI (système d'information) est soumis aux contraintes réglementaires et aux besoins issus de leurs différentes missions (formation tout au long de la vie, recherche et innovation notamment). Très vite ils ont compris qu'ils n'auraient pas les ressources pour tout développer localement dans chaque établissement et qu'il était profitable à tous de se regrouper pour mutualiser les développements. \Ulille adhère ainsi à l'agence de mutualisation des universités et établissements d'enseignement supérieur ou de recherche et de support à l'enseignement supérieur ou à la recherche (AMUE\site{http://amue.fr/}) qui lui fournit des briques essentielles de son SI, et à l'association ESUP-Portail\site{https://www.esup-portail.org/} qui développe des logiciels libres complémentaires à ceux de l'AMUE.

Le SI de \ulille est ainsi basé sur un assemblage de logiciels de l'AMUE, de logiciels libres, de logiciels commerciaux, et de développements locaux. L'hébergement des services peut être sur le poste de l'utilisateur, dans le datacentre de l'université ou chez un prestataire (logiciel comme un service dans le cloud). Le choix de la méthode de développement et du type d'hébergement est multicritère (couverture des besoins fonctionnels, coûts financiers du déploiement et de la maintenance, compétences et ressources disponibles, impacts environnementaux et sociaux, sécurité, etc) et fait l'objet d'un choix concerté entr toutes les parties prenantes de chaque projet. La \dnum assure la cohérence et la bonne intégration de tous ces projets.

\Ulille collabore en particulier avec les autres établissements d'enseignement supérieur et de recherche de la région Hauts-de-France au sein de la mission numérique enseignement supérieur et recherche Hauts-de-France pour porter en commun certains projets (datacentre, réseau d'accès à RENATER, logiciel antiplagiat, carte multiservices, etc). \Ulille assure le portage administratif de ces projets communs au service des établissements partenaires de la mission numérique.

\subsection{Ouverture et interopérabilité}
\Ulille est fortement engagée dans une politique de science et d'éducation ouverte. Cette politique repose beaucoup sur le numérique qui permet le partage à l'identique de toutes sortes de données. En effet, les données numériques sont dites «~non rivales~» car on peut en donner une copie parfaite tout en les conservant (contrairement à un objet physique, comme un livre par exemple, qu'on n'a plus quand on l'a donné). Une telle politique d'ouverture est fortement encouragée par l'Union Européenne\site{https://ec.europa.eu/jrc/en/open-education} et le gouvernement français\site{https://www.ouvrirlascience.fr/} (suite à la loi pour une république numérique du 7 octobre 2016\site[loirepnum]{https://www.economie.gouv.fr/republique-numerique}) et repose sur le principe que, comme le citoyen finance les universités publiques à travers ses impôts, il doit pouvoir avoir accès à leurs productions gratuitement.

Cette ouverture se traduit dans de nombreuses dimensions :
\begin{itemize}
  \item les publications ouvertes : \ulille a mis en place une archive ouverte institutionnelle, LillOA\site{https://lilloa.univ-lille.fr/}, et un portail HAL\site{https://hal.univ-lille.fr/}, et incite très fortement ses chercheurs à y déposer leurs productions ;
  \item les données ouvertes : \ulille héberge plusieurs plateformes de données de la recherche : le centre de services et de données Icare\site{https://www.icare.univ-lille.fr/} qui participe à l'infrastructure nationale Aeris, et la plateforme universitaire des données de Lille qui participe à l'infrastructure nationale PROGEDO\site{http://www.progedo.fr/} ;
  \item les ressources éducatives libres : \ulille est membre de chacune des 8 universités numériques thématiques\site{http://univ-numerique.fr/} (en particulier, elle héberge Unisciel\site{http://www.unisciel.fr/}, l'université des sciences en ligne, qui est un service inter-universitaire de \ulille), de FUN-MOOC\site{https://www.fun-mooc.fr/} et du consortium Ikigaï\site{http://www.ikigai.games/}. Elle participe ainsi activement à la production de ressources éducatives libres, les communs de l'éducation ;
  \item les logiciels libres, aussi bien dans leur utilisation que dans leur conception, voir l'encadré page~\pageref{cadre:ll}.
\end{itemize}

\cadre{Logiciels libres}{\label{cadre:ll}
\Ulille utilise très massivement les logiciels libres dans son infrastructure numérique (GNU/Linux, CAS, ...), ses outils de travail collaboratifs (Zimbra, Nextcloud, Collabora, Nuxeo, ...), ses plateformes d'enseignement (Moodle, BigBlueButton, ...), ses plateformes de calcul scientifique (OpenStack), ses sites web (Drupal, Typo3, POD), etc. Ces logiciels lui offrent adaptabilité, fiabilité, sécurité, maîtrise des coûts, contrôle et pérennité des données. En outre, les logiciels libres sont un excellent outil d'émancipation et d'éducation du citoyen, de maîtrise des données sensibles et d'indépendance, valeurs au cœur de l'Université.

Pour toutes ces raisons et conformément aux recommandations de la circulaire du 19 septembre 2012 du premier ministre Jean-Marc Ayrault\site{http://circulaire.legifrance.gouv.fr/pdf/2012/09/cir_35837.pdf} et à l'article 9 de la loi ESR de 2013\site{https://www.legifrance.gouv.fr/jorf/article_jo/JORFARTI000027735194}, \ulille utilise prioritairement des logiciels libres, en particulier ceux du socle interministériel des logiciels libres\site{http://references.modernisation.gouv.fr/socle-logiciels-libres}.

Notons en outre que des décisions récentes d'institutions européennes posent des questions importantes sur la légalité de la protection des données personnelles par les services soumis à la législation des États-Unis d'Amérique (rapport du 2 juillet 2020 du Contrôleur européen de la protection des données\site{https://edps.europa.eu/sites/edp/files/publication/20-07-02_edps_euis_microsoft_contract_investigation_en.html} et arrêté du 16 juillet 2020 de la Cour de justice de l'union européenne\site{https://curia.europa.eu/jcms/upload/docs/application/pdf/2020-07/cp200091fr.pdf}), ce qui renforce l'intérêt d'utiliser des logiciels libres et de contrôler l'hébergement des données.

Enfin, \ulille participe au développement de nombreux logiciels libres, notamment dans le cadre de ses activités de recherche, mais pas seulement. Conformément à la politique de contribution aux logiciels libres de l'état\site{https://www.numerique.gouv.fr/publications/politique-logiciel-libre/}, \ulille encourage ses personnels à diffuser leurs productions logicielles sous licence libre. Parmi les plus visibles de ceux-ci, citons Pharo\site{https://consortium.pharo.org/}, ensemble d'outils pour le développement d'applications informatiques complexes, labellisé plateforme technologique de \ulille, et POD\site{https://www.esup-portail.org/wiki/display/ES/esup-pod}, logiciel de diffusion de vidéos porté par \ulille pour le consortium ESUP-Portail.}

Pour rendre l'ouverture et le partage possibles, il est nécessaire d'utiliser des formats de fichiers standards et ouverts comme indiqué dans la version 2 du  Règlement Général d’Interopérabilité\site{http://references.modernisation.gouv.fr/sites/default/files/Referentiel_General_Interoperabilite_V2.pdf} dont le décret d'application a été publié au journal officiel le 22 avril 2016, et dans la loi pour une république numérique du 7 octobre 2016\footref{loirepnum}. Outre le partage, l'ouverture des formats de fichiers assure l'interopérabilité des outils et la pérennité des données stockées dans ces fichiers, contrairement aux formats de fichiers propriétaires pour lesquels il n'existe aucune garantie de pérennité dans le temps. Ainsi, \ulille recommande l'utilisation de formats de fichiers ouverts, comme les formats Open Document qui sont proposés par défaut dans sa suite bureautique collaborative en ligne\site{https://nextcloud.univ-lille.fr/} ou encore le PDF pour les documents non destinés à être modifiés.

\subsection{Cybersécurité}
La sécurité du système d'information est primordiale et repose sur une politique spécifique : la PSSI (Politique de Sécurité du Système d'Information), qui est conforme à la réglementation\site{https://www.ssi.gouv.fr/administration/reglementation/}, a été validée par le conseil d'administration de \ulille le 12 mars 2020 et est disponible sur le site de l'université dédié à la sécurité de l'information\site{https://ssi.univ-lille.fr/}. Le RSSI (responsable de la sécurité du système d'information) de \ulille est chargé de son application sous la responsabilité du président de l'université.

Un des points clés de cette PSSI est la mise en place d'un SMSI (système de management de la sécurité de l'information) qui formalise une démarche d'amélioration continue de cette sécurité afin de faire face aux menaces évolutives qui pèsent sur le système d'information de toute organisation (les plus courantes en 2020 étant l'hameçonnage et les rançongiciels) et de protéger les informations sensibles de la recherche scientifique (conformément à la politique nationale de protection du potentiel scientifique et technique de la nation\site{http://www.sgdsn.gouv.fr/missions/protection-du-potentiel-scientifique-et-technique-de-la-nation/}).

\vfill
\centerline{\includegraphics[width=0.6\textwidth]{piliers.pdf}}
\vfill
\vfill
\newpage
\appendix
\section{Annexe : grands projets structurants en 2020}
Afin de tracer la trajectoire de transformation numérique de \ulille, nous listons dans cette annexe les grands projets structurants du numérique à \ulille à l'automne 2020, en donnant leurs lignes directrices, leur niveau de maturité et leur échéance de réalisation. De nombreux projets de moindre impact sont menés au jour le jour pour répondre aux besoins courants des personnels, étudiants et partenaires de \ulille.

\emph{Tous les services numériques offerts aux usagers de l'université sont disponibles via l'ENT\site{https://ent.univ-lille.fr/} (Espace Numérique de Travail) et les documentations et tutoriels d'utilisation sont regroupés sur Infotuto\site{https://infotuto.univ-lille.fr/}.}

\subsection{Infrastructure et services d'hébergement}
\projet{RAOUL}{en production}{2017--2030}{Pour offrir la connectivité Internet aux composantes de \ulille et à ses partenaires sur le territoire de la MEL, \ulille maintient et opère un réseau\site{https://raoul.univ-lille.fr/} métropolitain de fibres optiques qui connecte à très haut débit tous les sites métropolitains de l'enseignement supérieur et de la recherche avec une très grande qualité de service, RAOUL, le réseau d'accès optique de \ulille.}

\projet{Réseaux de campus}{bonne sur la plupart des campus, moyenne sur le campus de la cité scientifique}{2020-2025}{La qualité du câblage des bâtiments, leur équipement en bornes Wi-Fi et de leur connexion au réseau RAOUL conditionne la qualité perçue du réseau pour les usagers. La \dnum rénove régulièrement ces réseaux de campus et projette de rénover en particulier le réseau de la Cité Scientifique avec un financement demandé au CPER 2021--2027.}

\projet{Datacentre Hauts-de-France}{en attente de labellisation pour 2020-2021}{2025}{Conformément à la stratégie INFRANUM du MESRI, les établissements d'enseignement supérieur et de recherche de la région Hauts-de-France (universités, écoles, organismes de recherche, publics et privés) construisent ensemble un datacentre destiné à héberger la majorité des serveurs supports des services numériques de ces établissements. La salle d'hébergement choisie est la salle principale de \ulille sise au M4 sur le campus de la cité scientifique. Des travaux de rénovation de cette salle sont en cours et des moyens pour atteindre l'état de l'art de l'hébergement et pouvoir accueillir en toute confiance les services des autres établissements sont demandés dans le CPER 2021--2027.}

\projet{Équipement des salles de formation}{bonne pour les amphis, plus variée pour les autres salles}{2020--2022}{Un effort important a été consenti en 2020 pour équiper la plupart des amphithéâtres en matériel de projection vidéo (écran et ordinateur fixe) et de captation vidéo pour permettre des enseignements en comodalité présentiel/distanciel pour faire face aux contraintes dues à la crise sanitaire. L'équipement des autres types de salles de formation sera poursuivi en 2021 et 2022.}

\projet{Équipement des usagers}{bonne mais politique à clarifier pour les composantes et laboratoires}{2021}{\Ulille assume son obligation d'équiper ses personnels en outils de travail informatiques. Cet équipement est pris en charge par la \dnum pour les services centraux, et par les composantes et laboratoires pour leurs personnels. \Ulille privilégie les ordinateurs portables pour permettre le télétravail et allonge le cycle de remplacement des machines afin de limiter leur impact environnemental. La politique d'achat passe par le marché national Matinfo afin de réduire les coûts et impacts environnementaux (en effet cette réduction des impacts environnementaux est une exigence du marché Matinfo). Concernant les étudiants, \ulille prête des ordinateurs (1200 en 2020) et alloue des moyens financiers pour la connexion réseau sur critères sociaux afin de lutter contre la fracture numérique chez les étudiants défavorisés.}

\subsection{Système d'information}
\projet{FTLV (formation tout au long de la vie)}{système opérationnel mais nécessitant encore une phase de renouvellement et de convergence de plusieurs composants}{2021}{Le système d'information supportant les activités d'enseignement de \ulille (SI FTLV) est probablement la partie la plus complexe du système d'information et celle qui manipule le plus de données. Suite à la fusion des 3 universités publiques lilloises, et pour permettre la mise en place d'une nouvelle offre de formation plus flexible, ce système d'information a été complètement repensé pour bien coordonner les activités et assurer une grande fiabilité des données (dossiers étudiants, description de l'offre de formation, données de scolarité, gestion des salles, gestion des groupes, gestion des services des enseignants, formation continue, alternance, etc). Plusieurs briques importantes de ce système sont en cours de renouvellement et d'uniformisation sur tous les campus. Ce travail de renouvellement et d'uniformisation se poursuit en 2020 et 2021 et doit permettre de stabiliser le SI FTLV en attendant le déploiement du service Pégase\site{https://www.pc-scol.fr/loutil-pegase/}, en 2024 ou 2025, qui est en cours de construction par l'AMUE en partenariat avec plusieurs universités.}

\projet{Recherche}{en cours de construction}{2022--2024}{Le SI recherche est un point relativement faible du SI de l'université. Une des raisons en est la variété des laboratoires et de leurs tutelles. \Ulille a récemment déployé ADUM\site{https://www.adum.fr/} pour la gestion des thèses de doctorat, déploie un outil de gestion des contrats de recherche et propose un outil d'analyse bibliométrique\site{https://lillometrics.univ-lille.fr/} pour le pilotage de la recherche. Il manque aujourd'hui le déploiement d'un outil de gestion de la recherche au service des chercheurs, des directions de laboratoire et des services d'appui de l'université qui faciliterait les interactions entre les multiples acteurs de la recherche. Un tel outil est en cours de développement à l'AMUE en lien avec le CNRS, caplab\site{http://www.amue.fr/recherche/logiciels/caplab/}, et certains laboratoires utilisent un développement interne du laboratoire CRIStAL. \Ulille regarde de près ces développements pour un déploiement prochain.}

\projet{Pilotage et administration}{plusieurs chantiers en cours}{2022}{Pour pouvoir piloter équitablement et efficacement la répartition des moyens entre les composantes et les services centraux de l'université dans une démarche de subsidiarité, il faut disposer de données fiables et partagées sur leur composition et leur activité. \Ulille fait un travail de fond sur son SI administratif (RH, finances) et sur les outils d'aide à la décision pour pouvoir objectiver les indicateurs de pilotage. La brique RH du SI est en cours de remplacement par un logiciel plus moderne et mieux adapté à nos besoins, et la partie décisionnelle est en cours de mise en place. Au-delà des outils, il y a un travail d'harmonisation des structures de l'université dans toutes les briques du SI.

En parallèle de ces grands chantiers, \ulille est engagée dans une simplification administrative qui inclut la dématérialisation de ses procédures (exemples : inscription des étudiants, signature électronique, achats, délivrance d'attestations de diplômes numériques, etc). Ces chantiers doivent permettre à l'établissement d'être prêt pour gérer efficacement et équitablement l'établissement expérimental qui est en construction pour janvier 2022.}

\projet{Mobilité internationale}{projet initié}{2021--2022}{De nouveaux outils de gestion des mobilités internationales, entrantes et sortantes vont être déployés pour répondre aux besoins des projets Erasums Without Paper de l'Union Européenne. Une carte d'étudiant européenne\site{https://agence.erasmusplus.fr/carte-etudiante-europeenne/} fera partie de ces déploiements.}

\projet{Documentation}{bonne}{2020}{Le système de gestion documentaire unifié de \ulille, Lillocat\site{https://lillocat.univ-lille.fr/} a été déployé en 2020. Il a permis d'harmoniser les systèmes de toutes les bibliothèques de l'université\site{https://scd.univ-lille.fr} et d'améliorer significativement le service rendu aux usagers.}

\subsection{Outils de travail collaboratif}
\projet{Messageries}{très bonne}{2018--}{\Ulille a fait le choix d'opérer en interne sa messagerie de courrier électronique (logiciels libres Zimbra\site{https://zimbra.univ-lille.fr/} et Sympa\site{https://listes.univ-lille.fr/}). Cette solution apporte sécurité et contrôle des données échangées, réduction des coûts, ainsi que réduction de l'empreinte environnementale par diminution de l'utilisation du réseau pour les échanges internes.}

\projet{Suite bureautique collaborative}{bonne}{2019--}{\Ulille propose une suite bureautique collaborative intégrée à sa solution de synchronisation et partage de fichiers nextcloud\site{https://nextcloud.univ-lille.fr/}. Un an après son déploiement, le fonctionnement est très satisfaisant et le service apprécié.}

\projet{Gestion électronique de documents}{très bonne}{2017--}{Principalement pour ses besoins administratifs, \ulille propose à ses usagers une solution de gestion électronique de documents\site{https://ged.univ-lille.fr/} basée sur le logiciel libre nuxeo et qui sert, entre autres, de base documentaire pour l'intranet\site{https://intranet.univ-lille.fr/}.}

\projet{Outils de visioconférence}{bonne}{2020--}{Avant la crise sanitaire de 2020, \ulille se reposait sur les services de RENATER pour ses besoins de visioconférence\site{https://renavisio.renater.fr/} et de web-conférence\site{https://rendez-vous.renater.fr/}. Ces besoins ayant fortement augmenté, nous avons complété ces services par une solution interne libre BigBlueButton (un serveur dédié à l'intégration de classes virtuelles dans Moodle\site{https://infotuto.univ-lille.fr/fiche/classe-virtuelle-bbb-via-moodle}, un serveur proposant des salons d'audio-conférence\site{https://bbb-2.univ-lille.fr/}, et un serveur proposant des salons de vidéo-conférence\site{https://bbb-3.univ-lille.fr/}) et une licence de site Zoom\site{https://univ-lille-fr.zoom.us/} pour les visioconférences et webinaires avec un grand nombre de participants. La complémentarité entre ces différents services devrait permettre de répondre à l'ensemble des cas d'usage.}

\subsection{Outils numériques pour la formation}
\projet{Plateforme pédagogique}{très bonne}{2019--}{La plateforme pédagogique Moodle\site{https://moodle.univ-lille.fr/} de \ulille permet aux enseignants de gérer l'ensemble des ressources et activités pédagogiques numériques de leurs enseignements. Elle est le point d'accès unique et uniformisé vers de nombreux services numériques pour l'enseignement. Ses capacités sont en permanence renforcées et uniformisées dans un soucis de facilité d'utilisation et de performances. En complément, la suite éditoriale Scenari\site{https://scenari.org/} est proposée aux enseignants pour le développement de leur modules de formation avec l'accompagnement des experts de la Direction de l'Innovation Pédagogique.}

\projet{Outils pour des pédagogies hybrides et actives}{bonne}{2019--}{\Ulille propose à ses enseignants  des outils pour renforcer l'engagement des étudiants dans leur apprentissage par des pédagogies hybrides et actives : Wooclap\site{https://app.wooclap.com/} (suite complète d'outils d'interactivité), 2reply\site{https://toreply.univ-lille.fr/} (système de vote très simple) et Padlet\site{https://fr.padlet.com/} (outil de partage de contenu). La direction de l'innovation pédagogique\site{https://dip.univ-lille.fr/} accompagne à l'usage pédagogique de ces outils.}

\projet{Analyse des traces d'apprentissage}{expérimentation en cours}{2019--}{Dans l'objectif d'aider les étudiants dans leur parcours d'apprentissage et les enseignants dans leur compréhension du processus d'apprentissage de leurs étudiants, \ulille expérimente des recherches-actions sur les analyses des traces d'apprentissages laissées par les étudiants dans leur utilisation des outils numériques. Cette expérimentation est menée en collaboration avec d'autres universités pour partager les connaissances sur les outils et les résultats des recherches menées. Les outils (basés sur le logiciel libre Apereo LAI\site{https://www.apereo.org/communities/learning-analytics-initiative}) sont déployés à l'échelle de l'université et ouverts à tous les enseignants désireux d'expérimenter leur utilisation.}

\projet{Lille Learning Lab}{financement acquis, cahier des charges établi}{2020-2021}{\Ulille équipe plusieurs salles sur plusieurs de ses campus de matériel audio-visuel innovant pour expérimenter de nouvelles formes d'enseignement, en particulier plus participatives, hybrides, ou à distance. Ce projet est soutenu financièrement par l'I-Site ULNE.}

\subsection{Outils pour la recherche}
\projet{Plateforme de calcul intensif}{très bonne}{2021--2027}{La politique de mutualisation des équipements scientifiques menée depuis plus de 10 ans a permis d'accroître fortement la puissance de calcul agrégée et proposée aux chercheurs de l’Université de Lille et de la région. Le mésocentre de calcul intensif\site{https://hpc.univ-lille.fr/} propose en effet un cluster de calcul à l'état de l'art (332~TFlops, 35~To RAM, 785~To de stockage), un cloud OpenStack (84~TFlops, 19~To RAM, 215~To de stockage ) et du stockage pérenne (2~Po). Le cloud est connecté aux fédérations de clouds nationale (FG-Cloud) et européenne (EGI Federated Cloud). Ces équipements, en constante évolution, seront largement renforcés dans les années à venir grâce à des financements CPER et AMI Equipex+ (projets SILECS et MesoNET).}

\projet{Archive ouverte institutionnelle LillOA}{bonne}{2018--2021}{\Ulille s'est dotée d'une archive ouverte institutionnelle\site{https://lilloa.univ-lille.fr/} pour soutenir sa politique de science ouverte. Cette plateforme est connectée à l'archive ouverte nationale HAL\site{https://hal.univ-lille.fr/} pour offrir un service complet aux laboratoires et renforcer la visibilité des publications de \ulille.}

\projet{Pages pro}{bonne, des améliorations en cours de développement}{2017--2022}{Les personnels de \ulille bénéficient de l'hébergement d'une page web professionnelle\site{https://pro.univ-lille.fr/} dont l'édition simple leur facilite la communication sur l'ensemble de leurs activités. Des fonctionnalités supplémentaires et une meilleure intégration avec les autres services numériques de l'université sont prévus dans les prochaines années.}

\subsection{Communication numérique}
\projet{Sites web et intranet}{bonne, base technique solide, principaux sites opérationnels, chantier permanent au gré des restructurations des composantes}{2018--2022}{Suite à la fusion des 3 université publiques lilloises en 2018 et à la restructuration des composantes de \ulille, il a un gros chantier de refonte des sites web\site{https://www.univ-lille.fr/}, ENT et intranets institutionnels. Ce chantier est l'occasion de proposer une uniformisation des sites, aussi bien sur le plan de l'identité visuelle que sur le plan technique pour faciliter leur réalisation, leur maintenance, leur sécurité, et la navigation entre ces différents sites. Cette uniformisation technique basée sur des logiciels libres a permis un gain de temps et une meilleure prise en compte des bonnes pratiques du web, sur le respect des standards, l'éco-conception et l'accessibilité.}

\projet{Application mobile}{projet initié}{2021}{Les sites web de \ulille sont adaptables aux écrans des smartphones, ce qui permet l'accès à l'information depuis ces terminaux mobiles. En complément, \ulille développe une application mobile dédiée à la vie étudiante pour faciliter la communication institutionnelle vers les étudiants en permettant de cibler cette communication par campus, composante ou formation. Cette application partagera une base technique commune avec les sites existants et permettra l'affichage de leurs contenus.}

\projet{Lilagora}{très bonne}{2018--2020}{\Ulille s'est dotée d'un réseau social des alumnis\site{https://www.lilagora.fr/} pour garder le lien avec la grande communauté des anciens de \ulille, faciliter la recherche de stages et le lien avec le tissu socio-économique.}

\subsection{Accompagnement des usagers et formation}
\projet{Soutien de proximité}{bonne}{2018--2021}{La \dnum accompagne les usagers de l'université sur le déploiement du matériel informatique, la gestion des postes et les usages des outils numériques. Cet accompagnement passe par des services de proximité sur tous les sites et une plateforme d'assistance\site{https://assistance.univ-lille.fr/} pour un suivi efficace des demandes. Un effort particulier est en cours pour augmenter le nombre et la qualité des documentations et tutoriels sur les services numériques en regroupant toutes ces aides sur un site unique, infotuto\site{https://infotuto.univ-lille.fr/}.}

\projet{Formation à la culture et aux compétences numériques}{en cours de déploiement}{2020--2021}{\Ulille a l'ambition de former tous ses étudiants et tous ses personnels à la culture et aux compétences numériques dans l'objectif d'en faire des citoyens éclairés et à l'aise dans notre monde qui est de plus en plus numérique. Un cours se déploie dans la nouvelle offre de formation dans toutes les licences sous diverses modalités (présentiel, hybride ou à distance) basé sur le référentiel européen DigComp\site{https://ec.europa.eu/jrc/en/digcomp}. Une fois les contenus et la pédagogie stabilisés, il sera mis dans l'offre de formation du personnel.}

\end{document}
